Chap 02
===========

## Overview

Haskell の簡単なサンプルコード

## Memo (Mac)
インストールとセットアップ方法のメモです

### Haskell

- brew install ghc
- 実行 runghc
- コンパイル ghc

### Cabal

- brew install haskell-cabal
- cd Chap02/SimpleExample
- 設定 cabal configure
- ビルド cabal build
- 実行 cabal run

### Stack

- brew install haskell-stack
- cd Chap02/Min
- ビルド stack build
- 実行 stack exec min

## Directories
    Chap02/
       Samples/         ＃2.0 でのサンプルコード
       SimpleExample/   ＃2.1 でのサンプルコード
       Min/             ＃2.1 でのサンプルコード
       README.md

### 2.0 sample codes

- Samples
 - runghc xxx.hs で動作
 - echo "tarai 20 10 0" | ghci Tarai.hs Tarai.hsは echo が必要
 - tarai.cpp は g++ -O2 -o tarai tarai.cpp でビルドが必要


